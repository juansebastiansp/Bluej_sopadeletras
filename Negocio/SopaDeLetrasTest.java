package Negocio;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SopaDeLetrasTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetrasTest
{
    /**
     * Default constructor for test class SopaDeLetrasTest
     */
    public SopaDeLetrasTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    { 

    }

    @Test
    public void crearMatriz() throws Exception
    {
        String palabras="marc,pe,a";
        SopaDeLetras s=new SopaDeLetras(palabras);

        char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};

        char esperado[][]=s.getSopas();
        // :) --> ideal == esperado

        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(true,pasoPrueba);

    }

    @Test
    public void crearMatriz_conError() throws Exception
    {
        String palabras="marc,pe,a";
        SopaDeLetras s=new SopaDeLetras();

        char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
        char esperado[][]=s.getSopas();
        // :) --> ideal == esperado

        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(false,pasoPrueba);

    }

    private boolean sonIguales(char m1[][], char m2[][])
    {

        if(m1==null || m2==null || m1.length !=m2.length)
            return false; 

        int cantFilas=m1.length; 

        for(int i=0;i<cantFilas;i++)
        {
            if( !sonIgualesVector(m1[i],m2[i]))
                return false;
        }

        return true;
    }

    private boolean sonIgualesVector(char v1[], char v2[])
    {
        if(v1.length != v2.length || v1==null || v2==null)
            return false;

        for(int j=0;j<v1.length;j++)
        {
            if(v1[j]!=v2[j])
                return false;
        }
        return true;
    }

    @Test
    public void esMatrizCuadrada1() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("ee,so");

        assertEquals(true,s.esCuadrada());
    }

    @Test
    public void esMatrizCuadrada2() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("palre,solar,ariaa,papas,loser");

        assertEquals(true,s.esCuadrada());
    }

    @Test
    public void esMatrizCuadrada3() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("pao,sla,los");

        assertEquals(true,s.esCuadrada());

    }

    @Test
    public void esMatrizCuadradaError1() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("eedow,ss,aaa");

        assertEquals(false,s.esCuadrada());
    }

    @Test
    public void esMatrizCuadradaError2() throws Exception
    {            
        SopaDeLetras s=new SopaDeLetras("esparta,marron,dinero,lombriz,saba");

        assertEquals(false,s.esCuadrada());
    }

    @Test
    public void esMatrizRectangular1() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("palo,sala,losa");

        assertEquals(true,s.esRectangular());
    }

    @Test
    public void esMatrizRectangular2() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("peri,sika,laza,patr,sdgg");

        assertEquals(true,s.esRectangular());
    }

    @Test
    public void esMatrizRectangular3() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("paor,slat,losa");

        assertEquals(true,s.esRectangular());

    }

    @Test
    public void esMatrizRectangularError1() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("dd,saa,ssdff");

        assertEquals(false,s.esRectangular());
    }

    @Test
    public void esMatrizRectangularError2() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("dedo,palaa,lloron");

        assertEquals(false,s.esRectangular());
    }

    @Test
    public void esMatrizDispersa1() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("sopa,seb,matriz,marco,lop");

        assertEquals(true,s.esDispersa());
    }

    @Test
    public void esMatrizDispersa2() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("peri,sikae,laa,pa,sadgg");

        assertEquals(true,s.esDispersa());
    }

    @Test
    public void esMatrizDispersa3() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("paoss,sla,losada,fe,alook");

        assertEquals(true,s.esDispersa());
    }

    @Test
    public void esMatrizDispersaError1() throws Exception
    {

        SopaDeLetras s=new SopaDeLetras("de,so");

        assertEquals(false,s.esDispersa());

    }

    @Test
    public void esMatrizDispersaError2() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("ded,thr");

        assertEquals(false,s.esDispersa());
    }

    @Test
    public void contarPalabras1() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("pepepepe,anitaaa,psaafaila,daealdal,soslo,pepes,ssaapda");

        assertEquals(4,s.getContar("pepe"));
    }

    @Test
    public void contarPalabras2() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("pepepepe,anitaaa,psaafaila,daealdal,soslo,pepes,ssaapda");

        assertEquals(8,s.getContar("p"));
    }

    @Test
    public void contarPalabras3() throws Exception
    {
        SopaDeLetras s=new SopaDeLetras("pepepepe,anitaaa,psaafaila,daealdal,soslo,pepes,ssaapda");

        assertEquals(4,s.getContar("aa"));
    }

    @Test
    public void contarPalabrasError1() throws Exception
    {
        try{
            SopaDeLetras s=new SopaDeLetras("pepepepe,anitaaa,psaafaila,daealdal,soslo,pepes,ssaapda");

            assertEquals(0,s.getContar(""));
        }catch(Exception e){
            String m="Error, ingrese una palabra o letra";
            assertEquals(m,e.getMessage());
        }
    }

    @Test
    public void contarPalabrasError2() throws Exception
    {
        try{
            SopaDeLetras s=new SopaDeLetras("");

            assertEquals(0,s.getContar("da"));
        }catch(Exception e){
            String m="Error no se puede crear la matriz de char para la sopa de letras";
            assertEquals(m,e.getMessage());
        }
    }

    @Test
    public void diagonalPrincipal1() throws Exception
    {
        SopaDeLetras so=new SopaDeLetras("palo,sala,losa,lata");

        char []p={'p','a','s','a'};

        assertEquals(true,this.sonIgualesVector(p,so.getDiagonalPrincipal()));
    }

    @Test
    public void diagonalPrincipal2() throws Exception
    {
        SopaDeLetras s1=new SopaDeLetras("est,seb,dio");

        char []q={'e','e','o'};

        assertEquals(true,this.sonIgualesVector(q,s1.getDiagonalPrincipal()));
    }

    @Test
    public void diagonalPrincipal3() throws Exception
    {
        SopaDeLetras s2=new SopaDeLetras("peri,sika,laza,pate");

        char []r={'p','i','z','r'};

        assertEquals(false,this.sonIgualesVector(r,s2.getDiagonalPrincipal()));
    }

    @Test
    public void diagonalPrincipalError1() throws Exception
    {
        try{
            SopaDeLetras s3=new SopaDeLetras();

            char []s={'p','a','l','e'};

            assertEquals(false,this.sonIgualesVector(s,s3.getDiagonalPrincipal()));
        }catch(Exception e){
            String m="Error, la matriz debe ser cuadrada y debe contener elementos";
            assertEquals(m,e.getMessage());
        }
    }

    @Test
    public void diagonalPrincipalError2() throws Exception
    {
        try{
            SopaDeLetras s4=new SopaDeLetras("san,pal,depee");

            char []t={'h','e','r','m'};

            assertEquals(false,this.sonIgualesVector(t,s4.getDiagonalPrincipal()));

        }catch(Exception e){
            String m="Error, la matriz debe ser cuadrada y debe contener elementos";
            assertEquals(m,e.getMessage());
        }
    }

}

