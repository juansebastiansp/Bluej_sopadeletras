package Negocio;

/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (Juan Sebastián Sánchez Prada-1151771) 
 *         (Brayan Camilo Galvan-1151776)
 * @version (a version number or a date)
 */
public class SopaDeLetras {

    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras() {

    }

    public SopaDeLetras(String palabras) throws Exception {
        if (palabras == null || palabras.isEmpty()) {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabras2[] = palabras.split(",");
        this.sopas = new char[palabras2.length][];
        int i = 0;
        for (String palabraX : palabras2) {
            //Creando las columnas de la fila i
            this.sopas[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopas[i]);
           i++;

        }

    }

    private void pasar(String palabra, char fila[]) {

        for (int j = 0; j < palabra.length(); j++) {
            fila[j] = palabra.charAt(j);
        }
    }

    @Override
    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                msg += this.sopas[i][j] + "\t";
            }
            msg += "\n";
        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char filas[] : this.sopas) {
            for (char dato : filas) {
                msg += dato + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public boolean esCuadrada(){        
        for (int i = 0; i < this.sopas.length; i++) {
            if (this.sopas.length != this.sopas[i].length) {
                return false;
            }
        }
        return true;
    }

    public boolean esDispersa(){
        return !(this.esCuadrada() || this.esRectangular());
    }

    public boolean esRectangular(){
        int x = this.sopas[0].length;
        for (int i = 0; i < this.sopas.length; i++) {
            if (this.sopas.length == x || x != this.sopas[i].length) {
                return false;
            }
            x = this.sopas[i].length;
        }
        return true;
    }

    /*
    retorna cuantas veces esta la palabra en la matriz
     */
    public int getContar(String palabra) throws Exception
    {
        if(palabra==null || palabra.isEmpty())
        throw new Exception("Error, ingrese una palabra o letra");
        
        int contador=0;
        char letras[] = palabra.toCharArray();
        for(int i=0;i<this.sopas.length;i++){
            contador+=getCantidadVector(letras, sopas[i]);   

        }
        return contador;
    }

    private int getCantidadVector(char letras[], char vector[])
    {   
        int contador=0;
        for(int i=0;i<vector.length;i++){
            if(esta(i,vector,letras)==true && (vector.length-i)>=letras.length){
                contador++;
            }
        }

        return contador;
    }

    private boolean esta(int posicionVector, char vector[],char letras[]){
        int posicionLetras=0;
        while(posicionVector<vector.length && posicionLetras<letras.length)
        {
            if (vector[posicionVector]!=letras[posicionLetras]){
                return false; 
            }
            posicionLetras++;
            posicionVector++;
        }
        return true;
    }

    /*
    debe ser cuadrada sopas
     */
    public char[] getDiagonalPrincipal() throws Exception {
        if (this.sopas==null||!this.esCuadrada()) 
            throw new Exception("Error, la matriz debe ser cuadrada y debe contener elementos");

        char diagonal[] = new char[this.sopas.length];

        for (int i = 0; i < this.sopas.length; i++) {
            diagonal[i] = this.sopas[i][i];
        }
        return diagonal;
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
    //!
}
